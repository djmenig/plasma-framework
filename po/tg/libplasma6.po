# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-framework package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-framework\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:13+0000\n"
"PO-Revision-Date: 2020-09-19 11:42+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:80
#, kde-format
msgid "More actions"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:568
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:568
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:46
#, kde-format
msgid "Password"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:68
#, kde-format
msgid "Search…"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:70
#, kde-format
msgid "Search"
msgstr ""

#: plasma/applet.cpp:295
#, kde-format
msgid "Unknown"
msgstr "Номаълум"

#: plasma/applet.cpp:769
#, kde-format
msgid "Activate %1 Widget"
msgstr "Фаъол кардани виҷети %1"

#: plasma/containment.cpp:95 plasma/private/applet_p.cpp:100
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Тоза кардани %1"

#: plasma/containment.cpp:101 plasma/corona.cpp:366 plasma/corona.cpp:460
#, kde-format
msgid "Enter Edit Mode"
msgstr "Гузариш ба реҷаи тағйирдиҳӣ"

#: plasma/containment.cpp:104 plasma/private/applet_p.cpp:105
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Танзимоти %1..."

#: plasma/corona.cpp:315 plasma/corona.cpp:447
#, kde-format
msgid "Lock Widgets"
msgstr "Қулф кардани виҷетҳо"

#: plasma/corona.cpp:315
#, kde-format
msgid "Unlock Widgets"
msgstr "Кушодани қулфи виҷетҳо"

#: plasma/corona.cpp:364
#, kde-format
msgid "Exit Edit Mode"
msgstr "Баромад аз реҷаи тағйирдиҳӣ"

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr ""
"Имконияти эҷодкунии зерҳофизаи мавзӯъро дар диски компютерӣ муайян мекунад."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Андозаи калонтарини зерҳофизаи мавзӯъ дар диски компютерӣ дар килобайтҳо "
"ҳисоб карда мешавад. Ба назар гиред, ки файлҳо алоҳида нигоҳ дошта мешаванд, "
"бинобар ин андозаи калонтарин метавонад истифода бурда нашавад. Танзимкунии "
"андозаи калонтар ба ҳамин сабаб безарар аст."

#: plasma/pluginloader.cpp:289 plasma/pluginloader.cpp:290
#, kde-format
msgctxt "misc category"
msgid "Miscellaneous"
msgstr "Барномаҳои гуногун"

#: plasma/private/applet_p.cpp:120
#, kde-format
msgid "The %1 widget did not define which ScriptEngine to use."
msgstr ""
"Виҷети %1 наметавонад, ки ScriptEngine-ро барои истифода муайян намояд."

#: plasma/private/applet_p.cpp:138
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr "Бастаи %1, ки барои виҷети %2 лозим аст, кушода нашуд."

#: plasma/private/applet_p.cpp:146
#, kde-format
msgid "Show Alternatives..."
msgstr "Намоиш додани имконоти дигар..."

#: plasma/private/applet_p.cpp:248
#, kde-format
msgid "Widget Removed"
msgstr "Виҷет тоза шуд"

#: plasma/private/applet_p.cpp:249
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Виҷети \"%1\" тоза карда шуд."

#: plasma/private/applet_p.cpp:253
#, kde-format
msgid "Panel Removed"
msgstr "Лавҳа тоза шуд"

#: plasma/private/applet_p.cpp:254
#, kde-format
msgid "A panel has been removed."
msgstr "Лавҳа тоза карда шуд."

#: plasma/private/applet_p.cpp:257
#, kde-format
msgid "Desktop Removed"
msgstr "Мизи корӣ тоза шуд"

#: plasma/private/applet_p.cpp:258
#, kde-format
msgid "A desktop has been removed."
msgstr "Мизи корӣ тоза карда шуд."

#: plasma/private/applet_p.cpp:261
#, kde-format
msgid "Undo"
msgstr "Ботил сохтан"

#: plasma/private/applet_p.cpp:353
#, kde-format
msgid "Widget Settings"
msgstr "Танзимоти виҷетҳо"

#: plasma/private/applet_p.cpp:359
#, kde-format
msgid "Remove this Widget"
msgstr "Тоза кардани ин виҷет"

#: plasma/private/containment_p.cpp:61
#, kde-format
msgid "Remove this Panel"
msgstr "Тоза кардани ин лавҳа"

#: plasma/private/containment_p.cpp:63
#, kde-format
msgid "Remove this Activity"
msgstr "Тоза кардани ин фаъолият"

#: plasma/private/containment_p.cpp:69
#, kde-format
msgid "Activity Settings"
msgstr "Танзимоти фаъолиятҳо"

#: plasma/private/containment_p.cpp:75
#, kde-format
msgid "Add Widgets..."
msgstr "Илова кардани виҷетҳо..."

#: plasma/private/containment_p.cpp:190
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Унсури дархостшуда ёфт нашуд: %1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"Мубодилакунии виҷет тавассути шабака иҷозат медиҳад. то тавонед ба ин виҷет "
"аз компютери дурдаст, монанди идоракунии дурдаст, дастрасӣ пайдо кунед."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Мубодилакунии ин виҷет тавассути шабака"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr ""
"Иҷозат диҳед, то ки ҳар як кас тавонад ба ин виҷет дастрасӣ пайдо кунад"

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Хидмати (null) беэътибор ягон амалиётро иҷро карда наметавонад."

#: plasmaquick/appletquickitem.cpp:526
#, kde-format
msgid "The root item of %1 must be of type ContaimentItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:531
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:538
#, fuzzy, kde-format
#| msgid "Unknown"
msgid "Unknown Applet"
msgstr "Номаълум"

#: plasmaquick/appletquickitem.cpp:554
#, fuzzy, kde-format
#| msgid "Error loading QML file: %1"
msgid "Error loading QML file: %1 %2"
msgstr "Хатои боркунии файли QML: %1"

#: plasmaquick/appletquickitem.cpp:556
#, kde-format
msgid "Error loading Applet: package inexistent. %1"
msgstr "Хатои боркунии зербарнома: баста вуҷуд надорад. %1"

#: plasmaquick/configview.cpp:102
#, kde-format
msgid "%1 Settings"
msgstr "Танзимоти %1"

#: plasmaquick/plasmoid/containmentinterface.cpp:546
#, kde-format
msgid "Plasma Package"
msgstr "Бастаи Plasma"

#: plasmaquick/plasmoid/containmentinterface.cpp:550
#, kde-format
msgid "Install"
msgstr "Насб кардан"

#: plasmaquick/plasmoid/containmentinterface.cpp:561
#, kde-format
msgid "Package Installation Failed"
msgstr "Насбкунии баста қатъ шуд"

#: plasmaquick/plasmoid/containmentinterface.cpp:578
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "Бастае, ки шумо интихоб кардед, беэътибор аст."

#: plasmaquick/plasmoid/containmentinterface.cpp:587
#: plasmaquick/plasmoid/containmentinterface.cpp:656
#, kde-format
msgid "Widgets"
msgstr "Виҷетҳо"

#: plasmaquick/plasmoid/containmentinterface.cpp:592
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Илова кардани %1"

#: plasmaquick/plasmoid/containmentinterface.cpp:606
#: plasmaquick/plasmoid/containmentinterface.cpp:660
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Илова кардани нишона"

#: plasmaquick/plasmoid/containmentinterface.cpp:618
#, kde-format
msgid "Wallpaper"
msgstr "Тасвири замина"

#: plasmaquick/plasmoid/containmentinterface.cpp:628
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Танзим кардани %1"

#: plasmaquick/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Ҳаракаткунии муҳтаво"

#~ msgid "Main Script File"
#~ msgstr "Файли нақши асосӣ"

#~ msgid "Tests"
#~ msgstr "Санҷишҳо"

#~ msgid "Images"
#~ msgstr "Тасвирҳо"

#~ msgid "Themed Images"
#~ msgstr "Тасвирҳои мавзӯӣ"

#~ msgid "Configuration Definitions"
#~ msgstr "Таърифҳои танзимот"

#~ msgid "User Interface"
#~ msgstr "Воситаи корбарӣ"

#~ msgid "Data Files"
#~ msgstr "Файлҳои иттилоотӣ"

#~ msgid "Executable Scripts"
#~ msgstr "Нақшҳои иҷрошаванда"

#~ msgid "Screenshot"
#~ msgstr "Акси экран"

#~ msgid "Translations"
#~ msgstr "Тарҷумаҳо"

#~ msgid "Configuration UI pages model"
#~ msgstr "Танзимоти намунаи саҳифаҳои UI"

#~ msgid "Configuration XML file"
#~ msgstr "Файли танзимии XML"

#~ msgid "Custom expander for compact applets"
#~ msgstr "Васеъкунандаи фармоишӣ барои зербарномаҳои ҷафс"

#~ msgid "Images for dialogs"
#~ msgstr "Тасвирҳо барои равзанаҳои гуфтугӯ"

#~ msgid "Generic dialog background"
#~ msgstr "Тасвири заминаи равзанаи гуфтугӯи умумӣ"

#~ msgid "Theme for the logout dialog"
#~ msgstr "Мавзӯъ барои равзанаи гуфтугӯи баромад аз низом"

#~ msgid "Wallpaper packages"
#~ msgstr "Бастаҳои тасвири замина"

#~ msgid "Images for widgets"
#~ msgstr "Тасвирҳо барои виҷетҳо"

#~ msgid "Background image for widgets"
#~ msgstr "Тасвири пасзамина барои виҷетҳо"

#~ msgid "Analog clock face"
#~ msgstr "Лавҳаи соати рақамӣ"

#~ msgid "Background image for panels"
#~ msgstr "Тасвири пасзамина барои лавҳаҳо"

#~ msgid "Background for graphing widgets"
#~ msgstr "Пасзамина барои виҷетҳои намудсозӣ"

#~ msgid "Background image for tooltips"
#~ msgstr "Тасвири пасзамина барои маслиҳатҳо"

#~ msgid "Opaque images for dialogs"
#~ msgstr "Тасвирҳои нoшаффoф барои равзанаҳои гуфтугӯ"

#~ msgid "Opaque generic dialog background"
#~ msgstr "Тасвири заминаи нoшаффoфи равзанаи гуфтугӯи умумӣ"

#~ msgid "Opaque theme for the logout dialog"
#~ msgstr "Мавзӯи нoшаффoф барои равзанаи гуфтугӯи баромад аз низом"

#~ msgid "Opaque images for widgets"
#~ msgstr "Тасвирҳои нoшаффoф барои виҷетҳо"

#~ msgid "Opaque background image for panels"
#~ msgstr "Тасвири пасзаминаи нoшаффoф барои лавҳаҳо"

#~ msgid "Opaque background image for tooltips"
#~ msgstr "Тасвири пасзаминаи нoшаффoф барои маслиҳатҳо"

#~ msgid "KColorScheme configuration file"
#~ msgstr "Файли танзимии KColorScheme"

#~ msgid "Service Descriptions"
#~ msgstr "Тавсифи хидматҳо"

#~ msgctxt ""
#~ "API or programming language the widget was written in, name of the widget"
#~ msgid "Could not create a %1 ScriptEngine for the %2 widget."
#~ msgstr "%1 ScriptEngine барои виҷети %2 эҷод карда нашуд."

#~ msgid "Script initialization failed"
#~ msgstr "Омодасозии нақш қатъ шуд"

#~ msgctxt "Agenda listview section title"
#~ msgid "Holidays"
#~ msgstr "Идҳо"

#~ msgctxt "Agenda listview section title"
#~ msgid "Events"
#~ msgstr "Рӯйдодҳо"

#~ msgctxt "Agenda listview section title"
#~ msgid "Todo"
#~ msgstr "Рӯйхати вазифаҳо"

#~ msgctxt "Means 'Other calendar items'"
#~ msgid "Other"
#~ msgstr "Дигар"

#~ msgid "Previous Month"
#~ msgstr "Моҳи гузашта"

#~ msgid "Previous Year"
#~ msgstr "Соли гузашта"

#~ msgid "Previous Decade"
#~ msgstr "Даҳсолагии гузашта"

#~ msgctxt "Reset calendar to today"
#~ msgid "Today"
#~ msgstr "Имрӯз"

#~ msgid "Reset calendar to today"
#~ msgstr "Тақвимро ба имрӯз бозсозӣ кунед"

#~ msgid "Next Month"
#~ msgstr "Моҳи оянда"

#~ msgid "Next Year"
#~ msgstr "Соли оянда"

#~ msgid "Next Decade"
#~ msgstr "Даҳсолагии оянда"

#, fuzzy
#~| msgid "Next Month"
#~ msgid "Months"
#~ msgstr "Моҳи оянда"

#~ msgid "OK"
#~ msgstr "ХУБ"

#~ msgid "Cancel"
#~ msgstr "Бекор кардан"

#~ msgid "Run the Associated Application"
#~ msgstr "Иҷро кардани барномаи марбут"

#~ msgid "Open with %1"
#~ msgstr "Кушодан ба воситаи %1"

#~ msgid "Accessibility"
#~ msgstr "Қобилияти дастрасӣ"

#~ msgid "Application Launchers"
#~ msgstr "Оғозкунандаи барномаҳо"

#~ msgid "Astronomy"
#~ msgstr "Ситopашинoсӣ"

#~ msgid "Date and Time"
#~ msgstr "Вақт ва сана"

#~ msgid "Development Tools"
#~ msgstr "Абзорҳои барномарезӣ"

#~ msgid "Education"
#~ msgstr "Илму маърифат"

#~ msgid "Environment and Weather"
#~ msgstr "Муҳити атроф ва обу ҳаво"

#~ msgid "Examples"
#~ msgstr "Мисолҳо"

#~ msgid "File System"
#~ msgstr "Низоми файлӣ"

#~ msgid "Fun and Games"
#~ msgstr "Дилхушӣ ва бозиҳо"

#~ msgid "Graphics"
#~ msgstr "Графика"

#~ msgid "Language"
#~ msgstr "Забон"

#~ msgid "Mapping"
#~ msgstr "Нақшакашӣ"

#~ msgid "Miscellaneous"
#~ msgstr "Барномаҳои гуногун"

#~ msgid "Multimedia"
#~ msgstr "Мултимедиа"

#~ msgid "Online Services"
#~ msgstr "Хизматҳои онлайн"

#~ msgid "Productivity"
#~ msgstr "Самаранокӣ"

#~ msgid "System Information"
#~ msgstr "Иттилооти низом"

#~ msgid "Utilities"
#~ msgstr "Барномаҳои муфид"

#~ msgid "Windows and Tasks"
#~ msgstr "Равзанаҳо ва вазифаҳо"

#~ msgid "Clipboard"
#~ msgstr "Ҳофизаи муваққатӣ"

#~ msgid "Tasks"
#~ msgstr "Вазифаҳо"

#~ msgctxt "%1 is the name of the containment"
#~ msgid "Edit %1..."
#~ msgstr "Таҳрир кардани %1..."

#~ msgid "Default settings for theme, etc."
#~ msgstr "Танзимоти стандартӣ барои мавзӯъ ва ғайра."

#~ msgid "Color scheme to use for applications."
#~ msgstr "Нақшаи рангҳо барои истифода дар барномаҳо."

#~ msgid "Preview Images"
#~ msgstr "Пешнамоиши тасвирҳо"

#~ msgid "Preview for the Login Manager"
#~ msgstr "Пешнамоиш барои мудири воридшавӣ"

#~ msgid "Preview for the Lock Screen"
#~ msgstr "Пешнамоиш барои экрани қулф"

#~ msgid "Preview for the Userswitcher"
#~ msgstr "Пешнамоиш барои ивазкунандаи корбар"

#~ msgid "Preview for the Virtual Desktop Switcher"
#~ msgstr "Пешнамоиш барои ивазкунандаи мизи кории виртуалӣ"

#~ msgid "Preview for Splash Screen"
#~ msgstr "Пешнамоиш барои экрани дурахшон"

#~ msgid "Preview for KRunner"
#~ msgstr "Пешнамоиш барои KRunner"

#~ msgid "Preview for the Window Decorations"
#~ msgstr "Пешнамоиш барои ороишҳои равзана"

#~ msgid "Preview for Window Switcher"
#~ msgstr "Пешнамоиш барои ивазкунандаи равзана"

#~ msgid "Login Manager"
#~ msgstr "Мудири воридшавӣ"

#~ msgid "Main Script for Login Manager"
#~ msgstr "Нақши асосӣ барои мудири воридшавӣ"

#~ msgid "Logout Dialog"
#~ msgstr "Равзанаи гуфтугӯи баромад аз низом"

#~ msgid "Main Script for Logout Dialog"
#~ msgstr "Нақши асосӣ барои равзанаи гуфтугӯи баромад аз низом"

#~ msgid "Screenlocker"
#~ msgstr "Қулфкунандаи экран"

#~ msgid "Main Script for Lock Screen"
#~ msgstr "Нақши асосӣ барои экрани қулф"

#~ msgid "UI for fast user switching"
#~ msgstr "Воситаи корбарӣ барои ивазкунии тезкори корбар"

#~ msgid "Main Script for User Switcher"
#~ msgstr "Нақши асосӣ барои ивазкунандаи корбар"

#~ msgid "Virtual Desktop Switcher"
#~ msgstr "Ивазкунандаи мизи кории виртуалӣ"

#~ msgid "Main Script for Virtual Desktop Switcher"
#~ msgstr "Нақши асосӣ барои ивазкунандаи мизи кории виртуалӣ"

#~ msgid "On-Screen Display Notifications"
#~ msgstr "Намоиши огоҳиномаҳо дар экран"

#~ msgid "Main Script for On-Screen Display Notifications"
#~ msgstr "Нақши асосӣ барои намоиши огоҳиномаҳо дар экран"

#~ msgid "Splash Screen"
#~ msgstr "Экрани дурахшон"

#~ msgid "Main Script for Splash Screen"
#~ msgstr "Нақши асосӣ барои экрани дурахшон"

#~ msgid "KRunner UI"
#~ msgstr "Воситаи корбарии KRunner"

#~ msgid "Main Script KRunner"
#~ msgstr "Нақши асосии KRunner"

#~ msgid "Window Decoration"
#~ msgstr "Ороиши равзана"

#~ msgid "Main Script for Window Decoration"
#~ msgstr "Нақши асосӣ барои ороиши равзана"

#~ msgid "Window Switcher"
#~ msgstr "Ивазкунандаи равзана"

#~ msgid "Main Script for Window Switcher"
#~ msgstr "Нақши асосӣ барои ивазкунандаи равзана"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Анҷом додани тарҳбандии фармоишӣ"

#~ msgid "Customize Layout..."
#~ msgstr "Тарҳбандии фармоишӣ..."

#~ msgid "Fetching file type..."
#~ msgstr "Бозёбии навъи файл..."

#~ msgctxt "%1 is the name of the containment"
#~ msgid "%1 Options"
#~ msgstr "Имконоти %1"
